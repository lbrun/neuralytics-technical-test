export default {
  methods: {
    ageRange(age) {
      if (age < 21) {
        return 'junior'
      }

      if (age < 39) {
        return 'medium'
      }

      return 'senior'
    },

    genderLabel(gender) {
      return gender === 1 ? 'male' : 'female'
    },

    familyStatusLabel(familyStatus) {
      switch (familyStatus) {
        case 1:
          return 'Exploitants agricoles'
        case 2:
          return 'Artisans, commerçants et chefs d\'entreprise'
        case 3:
          return 'Dirigeants et professions intellectuelles supérieures'
        case 4:
          return 'Professions intermédiaires'
        case 5:
          return 'Employés'
        case 6:
          return 'Ouvriers'
        case 7:
          return 'Retraité'
        case 8:
          return 'Autres personnes sans activité professionnelle'
      }
    }
  }
}
