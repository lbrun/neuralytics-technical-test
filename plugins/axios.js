import { setClient } from '~/services/PredictionService'

export default function ({ $axios, store }) {

  let canRefresh = true

  setClient($axios)

  $axios.onRequest(config => {
    if (store.getters['auth/token']) {
      config.headers['x-access-token'] = `${store.getters['auth/token']}`
    }
  })

  $axios.onResponseError(error => {
    if (error.response.status !== 403 || !canRefresh) {
      return Promise.reject(error)
    }

    canRefresh = false;

    return $axios.post('login', {
      email: process.env.API_LOGIN,
      password: process.env.API_PWD
    }).then(res => {
      canRefresh = true
      store.dispatch('auth/saveToken', res.data.token)

      const config = { url: error.config.url, method: error.config.method, data: error.config.data };
      config.headers = { 'x-access-token': res.data.token }
      return $axios(config)
    })
  })
}
