import Vue from 'vue'

Vue.filter('capitalizeFirst', value => {
  if (!value) {
    return ''
  }

  value = value.toLowerCase()
  return value.charAt(0).toUpperCase() + value.slice(1);
})

Vue.filter('pad', (value, size) => {
  value = String(value)
  while (value.length < (size || 2)) {
    value = '0' + value
  }
  return value
})

Vue.filter('plural', (value, count) => {
  value = String(value)
  return count > 1 ? (value + 's') : value
})
