const GENERIC_ERROR_MESSAGE = 'Une erreur s\'est produite.'

let client

const parseErrors = errors => {
  try {
    let key = Object.keys(errors).shift()
    return `${key}: ${errors[key].shift()}`
  } catch (e) {
    return GENERIC_ERROR_MESSAGE
  }
}

const parseData = ({ firstname, city, postal_code: postalCode, calcul_proba_complet: proba }) => {
  return {
    firstname,
    gender: parseProba(proba.SEXE),
    age: proba.AGE_MEAN ? Math.round(proba.AGE_MEAN) : parseProba(proba.AGEREVQ),
    postalCode,
    city,
    familyStatus: parseProba(proba.CS1),
    children: proba.EAC || 0
  }
}

const parseProba = proba => {
  let highest = { key: null, value: 0 }
  Object.keys(proba).forEach(key => {
    let value = proba[key]
    if (value > highest.value) {
      highest.key = key
      highest.value = value
    }
  })
  return parseInt(highest.key)
}

export function setClient(newClient) {
  client = newClient
}

export default {
  predict: (firstname, city, postalCode) => {
    var formData = new FormData();
    formData.set('firstname', firstname);
    formData.set('postal_code', postalCode);
    formData.set('city', city);

    return new Promise((resolve, reject) => {
      client
        .post(`/api/get/contact_data`, formData)
        .then(res => {
          res.data.result === false
            ? reject(parseErrors(res.data.errors))
            : resolve(parseData(res.data))
        })
        .catch(err => {
          reject(GENERIC_ERROR_MESSAGE)
        })
    })
  }
}
