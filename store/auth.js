export const state = () => ({
  token: null
})

export const mutations = {
  setToken(state, token) {
    state.token = token
  }
}

export const actions = {
  saveToken({ commit }, token) {
    this.$cookies.set('token', token, { path: '/', maxAge: 86400 })
    commit('setToken', token)
  },

  clearAuth({ commit }) {
    this.$cookies.remove('token')
  }
}

export const getters = {
  token: state => {
    return state.token
  }
}
