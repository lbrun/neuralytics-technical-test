const AVERAGE_STAY_TIME = 5 * 60000;
const MAXIMUM_CLIENTS = 5;

export const state = () => ({
  list: []
})

export const mutations = {
  add: (state, { profile, shelfId }) => {
    state.list.push({ profile, shelfId, date: new Date() })
  }
}

export const getters = {
  shelfClients: state => shelfId => {
    let max = new Date(new Date() - AVERAGE_STAY_TIME);
    let clients = state.list.filter(client => client.shelfId === shelfId && client.date > max)
    clients.sort((a, b) => a.date - b.date)
    return clients
  },

  waitingClientsCount: (state, getters) => shelfId => {
    let waiting = getters.shelfClients(shelfId).length - MAXIMUM_CLIENTS
    return waiting > 0 ? waiting : 0
  },

  waitingTime: (state, getters) => shelfId => {
    let now = new Date()
    let clients = getters.shelfClients(shelfId)
    if (clients.length < MAXIMUM_CLIENTS) {
      return 0
    }

    let slice = clients.slice(0, clients.length - MAXIMUM_CLIENTS + 1)
    return slice.reduce((acc, val) => {
      return acc + (AVERAGE_STAY_TIME - (now - val.date))
    }, 0)
  },

  isAvailable: (state, getters) => shelfId => {
    return getters.shelfClients(shelfId).length < MAXIMUM_CLIENTS
  }
}
