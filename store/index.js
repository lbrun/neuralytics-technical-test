export const state = () => ({})

export const mutations = {}

export const actions = {
  async nuxtServerInit({ commit, dispatch }, { req }) {
    const token = this.$cookies.get('token')
    if (!token) {
      this.$cookies.remove('token')
      return
    }
    commit('auth/setToken', token)
  }
}
