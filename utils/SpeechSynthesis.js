let init = false
if (process.client && 'speechSynthesis' in window) {
  var speech = new SpeechSynthesisUtterance();
  var voices = window.speechSynthesis.getVoices();
  init = true
}

const say = (message, rate = 10, pitch = 1) => {
  if (!init) {
    return false
  }

  speech.rate = rate / 10
  speech.pitch = pitch
  speech.text = message
  speechSynthesis.speak(speech)
  return true
}

const cancel = () => {
  speechSynthesis.cancel()
}

export default {
  say,
  cancel
}
