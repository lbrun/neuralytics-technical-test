export default {

  age: {
    junior: {
      facialHairChance: 0,
      clothes: ['overall'],
      mouth: ['smile'],
      eyes: ['happy'],
      accessories: ['round'],
    },

    medium: {
      facialHair: ['medium', 'light'],
      facialHairColor: ['auburn', 'black', 'blonde', 'brown'],
      clothes: ['hoodie', 'shirt'],
      mouth: ['smile'],
      eyes: ['defaultValue'],
      accessories: ['sunglasses'],
    },

    senior: {
      facialHair: ['fancy'],
      facialHairColor: ['gray'],
      hairColor: ['gray'],
      clothes: ['blazer'],
      mouth: ['serious'],
      eyes: ['defaultValue'],
      accessories: ['prescription02'],
    }
  },

  gender: {
    male: {
      top: ['shortHair'],
      hairColor: ['black', 'blonde', 'brown'],
      clothesColor: ['heather'],
      eyebrow: ['defaultValue']
    },

    female: {
      top: ['longHair'],
      hairColor: ['black', 'blonde', 'brown'],
      facialHairChance: 0,
      clothesColor: ['pink'],
      eyebrow: ['defaultValue']
    }
  }
}
