export default {

  male: {
    junior: {
      id: 'male-overalls',
      label: 'Salopette',
      itinerary: 'tout de suite sur votre droite'
    },

    medium: {
      id: 'male-sweats',
      label: 'Sweat à capuche',
      itinerary: 'au fond du magasin sur votre droite'
    },

    senior: {
      id: 'male-jackets',
      label: 'Vestes',
      itinerary: 'au centre du magasin sur votre droite'
    }
  },

  female: {
    junior: {
      id: 'female-overalls',
      label: 'Salopette',
      itinerary: 'tout de suite sur votre gauche'
    },

    medium: {
      id: 'female-pulls',
      label: 'Pull',
      itinerary: 'au centre du magasin sur votre gauche'
    },

    senior: {
      id: 'female-hats',
      label: 'Chapeaux',
      itinerary: 'au fond du magasin sur votre gauche'
    }
  }
}
